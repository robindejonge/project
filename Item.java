
/**
 * class Item - geef hier een beschrijving van deze class
 *
 * @author (jouw naam)
 * @version (versie nummer of datum)
 */
public class Item
{
    private static final String[] items = {
        "Slingshot", "Shortsword", "Boots of swiftness", "Buckler", "Chestplate"
    };
    private String itemName;
    private String itemDescription;
    private int itemStats;

    /**
     * Constructor voor objects van class Item
     */
    public Item(String name, String description, int stats)
    {
        itemName = name;
        itemDescription = description;
        itemStats = stats;
    }
      
    public String name()
    {
        return itemName;
    }
    
    public String description()
    {
        return itemDescription;
    }
    
    public int stats()
    {
        return itemStats;
    }
}
