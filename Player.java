/**
 * class Player - geef hier een beschrijving van deze class
 *
 * @author (jouw naam)
 * @version (versie nummer of datum)
 */
public class Player
{
  //public Room currentRoom;

  /**
   * Constructor voor objects van class Player
   */
  public Player()
  {
      
  }
    
  public void takeItem(Command command)
  {
      if(!command.hasSecondWord()) {
            System.out.println("What do you want to take?");
            return;
      } else {
            System.out.println("Ÿou picked up the " + command.getSecondWord());
      }
  }
}
