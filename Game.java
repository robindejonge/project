/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */

public class Game
{
    private Parser parser;
    private Room currentRoom, treasure1, treasure2;
    private Player player;
    private Item shortsword;

    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        createRooms();
        parser = new Parser();
        createNewItem();
        addItemToRoom();
        player = new Player();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        Room boss1, boss2, a1, a2, a3, a4, b1, b2, b3, b4, b5;
      
        // create the rooms
        boss1 = new Room("This is the Room where I fought that incredibly powerful monster...");
        boss2 = new Room("This is the Room where I fought that incredibly powerful monster...");
        treasure1 = new Room("This is the Room where I found this... item...");
        treasure2 = new Room("This is the Room where I found this... item...");
        a1 = new Room("This is the Room where I started...");
        a2 = new Room("The second Room I encountered");
        a3 = new Room("The third Room I encountered");
        a4 = new Room("The fourth and last 'normal' Room I will encounter on this floor");
        b1 = new Room("Second floor, first Room!");
        b2 = new Room("The second Room I encountered");
        b3 = new Room("The third Room I encountered");
        b4 = new Room("The fourth Room I encountered");
        b5 = new Room("The fifth and last 'normal' Room I will encounter on this floor");
        
        // initialise room exits
        boss1.setExit("north", a4);
        boss1.setExit("down", b1);
        boss2.setExit("north", b5);
        treasure1.setExit("east", a2);
        treasure2.setExit("east", b2);
        a1.setExit("south", a2);
        a2.setExit("north", a1);
        a2.setExit("south", a3);
        a2.setExit("west", treasure1);
        a3.setExit("north", a2);
        a3.setExit("south", a4);
        a4.setExit("north", a3);
        a4.setExit("south", boss1);
        b1.setExit("south", b2);
        b2.setExit("north", b1);
        b2.setExit("south", b3);
        b2.setExit("west", treasure2);
        b3.setExit("north", b2);
        b3.setExit("south", b4);
        b4.setExit("north", b3);
        b4.setExit("south", b5);
        b5.setExit("north", b4);
        b5.setExit("south", boss2);

        currentRoom = a1;  // start game in room 1
    }
    
    private void createNewItem()
    {
        shortsword = new Item("Shortsword", "A short but sharp sword", 20);
    }
    
    private void addItemToRoom()
    {
        treasure1.addItems(shortsword);
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
                
        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Scared already?");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("Welcome to Binding of Isaac!");
        System.out.println();
        System.out.println("It's dark outside. Evil lurks around the house and attempts");
        System.out.println("to get to Isaac. Unrecognizable shades are reflected against the walls.");
        System.out.println("Then Isaac wakes up. It's still midnigt and pitchblack in his bedroom.");
        System.out.println("His nightmare felt very real and became a fresh memory in his mind.");
        System.out.println("Suddenly Isaac is overwhelmed by an insane level of fear. Shades");
        System.out.println("are moving in front of the window.");
        System.out.println("Isaac runs away. There's knocking on the windows. Screams echo behind");
        System.out.println("Isaac as he runs down the stairs. Loud banging on the door. The");
        System.out.println("screams are deafening loud. they seem to be inside Isaac's head");
        System.out.println("He decides to flee into the basement. It looks less dark than outside.");
        System.out.println("On the other hand, no one knows what is in here. Time to explore...");
        System.out.println();
        System.out.println("Type 'help' if you need help.");
        System.out.println();
        printLocationInfo();
    }
    
    /**
     * @prints Room description.
     * @prints Available exits.
     */
    private void printLocationInfo()
    {
        System.out.println(currentRoom.getLongDescription());
        System.out.println();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("Unknown command!");
            return false;
        }

        String commandWord = command.getCommandWord();
        if (commandWord.equals("help")) {
            printHelp();
        }
        else if (commandWord.equals("go")) {
            goRoom(command);
        }
        else if (commandWord.equals("quit")) {
            wantToQuit = quit(command);
        }
        else if (commandWord.equals("look")) {
            look();
        }
        else if (commandWord.equals("take")) {
            player.takeItem(command);
        }

        return wantToQuit;
    }
    
    private void look()
    {
        System.out.println(currentRoom.getLongDescription());
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("Your goal is to pick the right risks for your character so he can");
        System.out.println("fight or flee his way through the dungeon. Be wary, fights are");
        System.out.println("inevitable and some enemies are just too strong for Isaac.");
        System.out.println();
        System.out.println("The following commands are at your disposal at this moment:");
        parser.getCommands();
    }

    /** 
     * Try to go in one direction. If there is an exit, enter
     * the new room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        //currentRoom = previousRoom;
        Room nextRoom = currentRoom.getExit(direction);

        if (nextRoom == null) {
            if(command.getSecondWord() != direction) {
                System.out.println("Here is just a solid wall. You should pick another direction.");
            } else {
                System.out.println("This is not a valid direction");
            }
        }
        else {
            currentRoom = nextRoom;
            System.out.println(currentRoom.getLongDescription());
        }
    }
    
    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}
