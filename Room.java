import java.util.HashMap;
import java.util.Set;
import java.util.ArrayList;
/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  The exits are labelled north, 
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */
public class Room 
{
    private String description;
    private HashMap<String, Room> exits;
    private ArrayList<Item> items;
    
    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    public Room(String description) 
    {
        this.description = description;
        exits = new HashMap<>();
        items = new ArrayList<>();
    }
    
    public String getLongDescription()
    {
        return description + ".\n" + getExitString() + ".\n" + getItemName() + getItemDescription();
    }
    
    private String getItemName()
    {
        String result = "";
        for(Item item : items) {
            result = item.name();
        }
        
        if(result != "") {
            return "There is a " + result + " in this Room. ";
        } else {
            return "";
        }
    }
    
    private String getItemDescription()
    {
        String result = "";
        for(Item item : items) {
            result = item.description();
        }
        
        if(result != "") {
            return "It's " + result;
        } else {
            return "";
        }
    }

    /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     */
    public void setExit(String direction, Room next) 
    {
        exits.put(direction, next);
    }
    
    public Room getExit(String direction)
    {
        return exits.get(direction);
    }
    
    
    /**
     * Retourneer een string met daarin de uitgangen van de ruimte.
     * @return Een omschrijving van de aanwezige uitgangen in de ruimte
     */
    public String getExitString()
    {
        String exitString = "Exits:";
        Set<String> keys = exits.keySet();
        for(String exit : keys) {
            exitString += " " + exit;
        }
        return exitString;
    }

    /**
     * @return The description of the room.
     */
    public String getDescription()
    {
        return description;
    }
    
    public void addItems(Item item)
    {
        items.add(item);
    }
}
